﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorTester : MonoBehaviour {
    public CharacterSensor sensor = new CharacterSensor();
    public Character sensation = null;

    public void Update()
    {
        sensation = sensor.SenseFirst(transform.position);
    }

    public void OnDrawGizmos()
    {
        sensor.DrawGizmos();
    }
}
