﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterSensor : Sensor<Character>
{

}
[System.Serializable]
public class ItemSensor : Sensor<ItemDrop>
{
    public List<ItemDrop.ItemTypes> ItemTypes = new List<ItemDrop.ItemTypes>();

    public override void Sense(Vector3 position)
    {
        if(ItemTypes.Count == 0)
        {
            return;
        }

        base.Sense(position);
        List<ItemDrop> newSensations = new List<ItemDrop>();
        foreach (var item in Sensations)
        {
            if(!item.IsEquipped)
            {
                if (ItemTypes.Contains(item.ItemType))
                {
                    newSensations.Add(item);
                }
            }
            
        }
        Sensations = newSensations;
    }
}


[System.Serializable]
public class Sensor<T> where T: Component {
    public Type type = Type.Ray;
    public Vector3 Origin = new Vector3();
    public Vector3 Direction = new Vector3();
    public float Distance = 1.0f;
    public LayerMask LayerMask = 0;
    public List<T> Sensations = new List<T>();
    public List<T> Ignore = new List<T>();
    List<Collider> _Colliders = new List<Collider>();
    Vector3 _position;
    Color debugColor = new Color(0, 1, 0, 0.2f);
    public enum Type
    {
        Ray,
        Sphere
    }

    public T SenseFirst(Vector3 position)
    {
        Sense(position);
        if(Sensations.Count > 0)
        {
            return Sensations[0];
        }

        return null;
    }

    public virtual void Sense(Vector3 position)
    {
        _position = position;
        _Colliders.Clear();
        Sensations.Clear();
        switch (type)
        {
            case Type.Ray:
                SenseRay();
                break;
            case Type.Sphere:
                SenseSphere();
                break;
            default:
                break;
        }

        foreach(var collider in _Colliders)
        {
            T sensation = collider.GetComponent<T>();
            if(sensation == null)
            {
                continue;
            }
            if(Ignore.Contains(sensation))
            {
                continue;
            }
            else
            {
                Sensations.Add(sensation);
            }
        }
    }

    void SenseRay()
    {
        _Colliders.Clear();
        var hits = Physics.RaycastAll(_position + Origin, Direction.normalized, Distance, LayerMask);
        foreach(var hit in hits)
        {
            _Colliders.Add(hit.collider);
        }
    }

    void SenseSphere()
    {
        _Colliders.Clear();
        _Colliders = new List<Collider>(Physics.OverlapSphere(_position + Origin, Distance, LayerMask));
    }

    public void DrawGizmos()
    {
        Gizmos.color = debugColor;
        switch (type)
        {
            case Type.Ray:
                GizmosDrawRay();
                break;
            case Type.Sphere:
                GizmosDrawSphere();
                break;
            default:
                break;
        }
    }

    public void GizmosDrawRay()
    {
        Gizmos.DrawLine(_position + Origin, _position + Origin + (Direction.normalized * Distance));
    }

    void GizmosDrawSphere()
    {
        Gizmos.DrawSphere(_position + Origin, Distance);
    }


}
