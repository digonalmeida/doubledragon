﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedArea : MonoBehaviour {

    public GameObject minPlayer, maxPlayer;
    public GameObject player;
    public float offset = 0.05f;

    public List<Character> enemies;

    public bool playerEntered = false;
    public bool enemiesDefeated = false;

    private float minPlayerX, maxPlayerX;

    private void Start()
    {
        minPlayerX = minPlayer.transform.position.x;
        maxPlayerX = maxPlayer.transform.position.x;
    }

    private void Update ()
    {
        if (!playerEntered)
        {
            CheckPlayerPosition();
        }
        else if (!enemiesDefeated) 
        {
            CheckEnemies();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void CheckPlayerPosition()
    {
        if (player.transform.position.x > minPlayerX + offset && player.transform.position.x < maxPlayerX - offset )
        {
            LockPlayer();
        }
    }

    private void CheckEnemies()
    {
        if (EnemiesDefeated())
        {
            ReleasekPlayer();
            ContinueHand.Instance.Show();
        }
    }

    private void LockPlayer()
    {
        //minPlayer.GetComponent<BoxCollider>().enabled = true;
        maxPlayer.GetComponent<BoxCollider>().enabled = true;
        Camera.main.gameObject.GetComponent<FollowPlayer>().SetPlayerBounds(minPlayerX, maxPlayerX);
        playerEntered = true;
    }

    private void ReleasekPlayer()
    {
        minPlayer.GetComponent<BoxCollider>().enabled = false;
        maxPlayer.GetComponent<BoxCollider>().enabled = false;
        Camera.main.gameObject.GetComponent<FollowPlayer>().FreePlayerBounds();
        enemiesDefeated = true;
    }

    private bool EnemiesDefeated()
    {
        for (int i=0; i < enemies.Count; i++)
        {
            if (enemies[i].Hp > 0)
            {
                return false;
            }
        }

        return true;
    }
}
