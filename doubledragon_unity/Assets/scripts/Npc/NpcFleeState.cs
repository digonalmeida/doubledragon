﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
public class NpcFleeState:  State<NpcController>{
    private Vector3 _MoveDirection = new Vector3();

    public override void Enter()
    {
        base.Enter();
        Agent.ResetRandomDeltaDistance();
        Agent.Character.Input.Punch = false;
        Agent.Character.Input.Kick = false;
        Agent.Character.Input.Jump = false;
        Agent.Character.Input.MovementDirection = Vector3.zero;
    }

    public override void Update()
    {
        if(Agent.PlayerCharacter != null)
        {
            _MoveDirection =  Agent.Character.transform.position - Agent.PlayerCharacter.transform.position;
            _MoveDirection.y = 0;
            _MoveDirection.Normalize();

        }
        else
        {
            _MoveDirection = Vector2.zero;
        }
        
        Agent.Character.Input.MovementDirection = _MoveDirection;
        Agent.Character.Input.LookDirection = -_MoveDirection;
    }
}
