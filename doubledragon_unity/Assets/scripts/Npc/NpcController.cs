﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
public class NpcController : MonoBehaviour {
    public Character Character;
    public Character PlayerCharacter;
    public List<ItemDrop.ItemTypes> WantedDropItems;
    
    FiniteStateMachine<NpcController> StateMachine;
    NpcIdleState IdleState = new NpcIdleState();
    NpcFollowState FollowState = new NpcFollowState();
    NpcFleeState FleeState = new NpcFleeState();
    NpcAttackState AttackState = new NpcAttackState();
    NpcGetWeaponState GetWeaponState = new NpcGetWeaponState();

    public float OverrideSpeed = 0.5f;
    public bool WantToAttack;
    public float WantToAttackTimeout = 2;

    public ItemDrop TargetItem { get; private set; }

    //enter /exit
    public float IdleToFollowDistance = 0.5f;
    public float FollowToIdleDistance = 1.0f;
    public float FollowToFleeDistance = 0.2f;
    public float FleeToFollowDistance = 0.5f;
    public float AttackDistance = 0.3f;


    private float _RandomDeltaDistance = 0;

    float _SqrDistanceToPlayer = 1000;
    float _SearchItemInterval = 0.5f; 

    public enum EventTriggers
    {
        Attack,
        ItemFound
    }

    public void ResetAttackTimer()
    {
        WantToAttackTimeout = Random.Range(0.5f, 2.0f);
    }

    public void ResetRandomDeltaDistance()
    {
        _RandomDeltaDistance = Random.Range(-0.1f, 0.1f);
    }

    public void Start()
    {
        ResetAttackTimer();
        Character.Speed = OverrideSpeed;
        StateMachine = new FiniteStateMachine<NpcController>(this);

        IdleState.AddCondition(CheckWantToGetItem, GetWeaponState);
        IdleState.AddCondition(CheckIdleToFollowDistance, FollowState);

        FollowState.AddCondition(CheckWantToGetItem, GetWeaponState);
        FollowState.AddCondition(CheckFollowToIdleDistance, IdleState);
        FollowState.AddCondition(CheckFollowToFleeDistance, FleeState);
        FollowState.AddCondition(CheckWantToAttack, AttackState);

        FleeState.AddCondition(CheckWantToGetItem, GetWeaponState);
        FleeState.AddCondition(CheckFleeToFollowDistance, FollowState);
        FleeState.AddCondition(CheckWantToAttack, AttackState);

        AttackState.AddCondition(CheckWantToGetItem, GetWeaponState);
        AttackState.AddCondition(CheckWantToStopAttack, IdleState);


        GetWeaponState.AddCondition(WantToStopGettingItem, IdleState);

        StateMachine.SetState(FollowState);
    }

    public bool WantToStopGettingItem()
    {
        return !CheckWantToGetItem();
    }
    public bool CheckWantToGetItem()
    {
        return Character.ItemState == ItemDrop.ItemTypes.None &&
            TargetItem != null;
    }
    public bool CheckIdleToFollowDistance()
    {
        return _SqrDistanceToPlayer < IdleToFollowDistance + _RandomDeltaDistance;
    }
    public bool CheckFollowToIdleDistance()
    {
        return _SqrDistanceToPlayer > FollowToIdleDistance + _RandomDeltaDistance;
    }
    public bool CheckFollowToFleeDistance()
    {
        return _SqrDistanceToPlayer < FollowToFleeDistance + _RandomDeltaDistance;
    }
    public bool CheckFleeToFollowDistance()
    {
        return _SqrDistanceToPlayer > FleeToFollowDistance + _RandomDeltaDistance;
    }
    public bool CheckWantToAttack()
    {
        return WantToAttack &&
            _SqrDistanceToPlayer < AttackDistance + _RandomDeltaDistance &&
            !PlayerCharacter.IsInvulnerable;
    }
    public bool CheckWantToStopAttack()
    {
        return !CheckWantToAttack();
    }

    public void Update()
    {

        if(Character != null && PlayerCharacter != null)
        {
            _SqrDistanceToPlayer = Vector2.SqrMagnitude(PlayerCharacter.transform.position - Character.transform.position);
        }
        SearchItem();
        StateMachine.Update();
    }

    public void SearchItem()
    {
        TargetItem = null;
        if(WantedDropItems.Count == 0)
        {
            return;
        }
        if(Character.ItemState != ItemDrop.ItemTypes.None)
        {
            return;
        }

        var drops = FindObjectsOfType<ItemDrop>();
        foreach(var drop in drops)
        {
            if(!drop.IsEquipped)
            {
                if(WantedDropItems.Contains(drop.ItemType))
                {
                    TargetItem = drop;
                }
            }
        }
    }
}
