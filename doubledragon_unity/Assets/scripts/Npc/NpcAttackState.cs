﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
public class NpcAttackState:  State<NpcController>{
    private Vector3 _MoveDirection = new Vector3();
    private float _JumpKickTimeout = 0.1f;
    private bool _Kicked = false;
    public enum AttackTypes
    {
        Punch,
        JumpKick,
        Count
    }
    AttackTypes _AttackType = AttackTypes.Punch;

    public void decideAttackType()
    {
        int t = Random.Range(0, (int)AttackTypes.Count);
        _AttackType = (AttackTypes)t;
    }

    public override void Enter()
    {
        Agent.ResetRandomDeltaDistance();
        decideAttackType();

        base.Enter();
        Agent.Character.Input.Punch = false;
        Agent.Character.Input.Kick = false;
        Agent.Character.Input.Jump = false;
        Agent.Character.Input.MovementDirection = Vector3.zero;

        switch (_AttackType)
        {
            case AttackTypes.Punch:
                Agent.Character.Input.Punch = true;
                break;
            case AttackTypes.JumpKick:
                _JumpKickTimeout = 0.1f;
                Agent.Character.Input.Jump = true;
                _Kicked = false;
                break;
        }

    }

    public override void Update()
    {
        if (Agent.PlayerCharacter != null)
        {
            _MoveDirection = Agent.PlayerCharacter.transform.position - Agent.Character.transform.position;
            _MoveDirection.Normalize();
        }
        else
        {
            _MoveDirection = Vector2.zero;
        }
        Agent.Character.Input.LookDirection = _MoveDirection;
        Agent.Character.Input.MovementDirection = _MoveDirection;

        switch (_AttackType)
        {
            case AttackTypes.Punch:
                break;
            case AttackTypes.JumpKick:
                if(!_Kicked)
                {
                    if (_JumpKickTimeout <= 0)
                    {
                        Agent.Character.Input.Jump = false;
                        Agent.Character.Input.Kick = true;
                        _Kicked = true;
                    }
                    _JumpKickTimeout -= Time.deltaTime;
                }
                
                break;
            default:
                break;
        }
    }
    public override void Exit()
    {
        base.Exit();

        switch (_AttackType)
        {
            case AttackTypes.Punch:
                Agent.Character.Input.Punch = false;
                Agent.Character.Input.MovementDirection = Vector3.zero;
                break;
            case AttackTypes.JumpKick:
                Agent.Character.Input.Kick = false;
                Agent.Character.Input.Jump = false;
                Agent.Character.Input.MovementDirection = Vector3.zero;
                break;
            default:
                break;
        }
        
        Agent.WantToAttack = false;
        Agent.ResetAttackTimer();
    }
}
