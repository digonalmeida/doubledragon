﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
public class NpcIdleState:  State<NpcController>{

    Vector2 _LookDirection;

    public override void Enter()
    {
        base.Enter();
        Agent.Character.Input.Punch = false;
        Agent.Character.Input.Kick = false;
        Agent.Character.Input.Jump = false;
        Agent.Character.Input.MovementDirection = Vector2.zero;
    }
    

    public override void Update()
    {
        base.Update();
        if(Agent.Character == null)
        {
            return;
        }
        _LookDirection = Agent.PlayerCharacter.transform.position - Agent.Character.transform.position;
        Agent.Character.Input.LookDirection = _LookDirection;
    }
}
