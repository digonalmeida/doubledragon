﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
public class NpcFollowState:  State<NpcController>{
    private Vector3 _MoveDirection = new Vector3();

    public override void Enter()
    {
        base.Enter();
        Agent.Character.Input.Punch = false;
        Agent.Character.Input.Kick = false;
        Agent.Character.Input.Jump = false;
        Agent.Character.Input.MovementDirection = Vector3.zero;
        Agent.ResetRandomDeltaDistance();
    }

    public override void Update()
    {
        Agent.WantToAttackTimeout -= Time.deltaTime;
        if (Agent.WantToAttackTimeout <= 0)
        {
            Agent.WantToAttack = true;
        }

        if (Agent.PlayerCharacter != null)
        {
            _MoveDirection = Agent.PlayerCharacter.transform.position - Agent.Character.transform.position;
            _MoveDirection.y = 0;
            _MoveDirection.Normalize();
        }
        else
        {
            _MoveDirection = Vector2.zero;
        }
        Agent.Character.Input.LookDirection = _MoveDirection;
        Agent.Character.Input.MovementDirection = _MoveDirection;
    }
}
