﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
public class NpcGetWeaponState:  State<NpcController>{

    public override void Enter()
    {
        base.Enter();
        Agent.Character.Input.Punch = false;
        Agent.Character.Input.Kick = false;
        Agent.Character.Input.Jump = false;
        Agent.Character.Input.MovementDirection = Vector3.zero;
    }

    public override void Update()
    {
        if(Agent.TargetItem == null)
        {
            return;
        }
        if(Agent.Character.NearbyItemType != ItemDrop.ItemTypes.None)
        {
            Agent.Character.Input.Punch = true;
            Agent.Character.Input.MovementDirection = Vector2.zero;
        }
        else
        {
            Agent.Character.Input.Punch = false;
            Agent.Character.Input.MovementDirection = (Agent.TargetItem.transform.position - Agent.Character.transform.position).normalized;
            Agent.Character.Input.LookDirection = Agent.Character.Input.MovementDirection;
        }
        
    }
}
