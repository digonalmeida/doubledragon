﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour {

    public float timeScale = 1f;
    public KeyCode timeKey;

    private bool _normalTime = true;

	void Start ()
    {
		
	}
	
	void Update ()
    {
        if (Input.GetKeyDown(timeKey))
        {
            if (_normalTime)
            {
                Time.timeScale = timeScale;
                _normalTime = false;
            }
            else
            {
                Time.timeScale = 1f;
                _normalTime = true;
            }
        }
	}
}
