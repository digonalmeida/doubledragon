﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform minCamera, maxCamera;
    public GameObject player;

    public float offset = 0.8f;
    public float smothSpeed = 10f;
    public bool limitedMovement = false;

    private float minCameraX, maxCameraX;
    private float minPlayerX, maxPlayerX;
    private float cameraDistanceX;
    private Vector3 pos;

    private void Start()
    {
        Camera camera = Camera.main;
        cameraDistanceX = camera.orthographicSize * camera.aspect;

        minCameraX = minCamera.position.x + cameraDistanceX;
        maxCameraX = maxCamera.position.x - cameraDistanceX;
    }

    private void LateUpdate()
    {
        FollowPlayerCharacter();
    }

    private void FollowPlayerCharacter()
    {
        pos = player.transform.position;

        if (pos.x <= transform.position.x + offset && pos.x >= transform.position.x - offset)
        {
            return;
        }
        else if (pos.x > transform.position.x + offset)
        {
            if (limitedMovement && transform.position.x + cameraDistanceX - 0.05 >= maxPlayerX)
            {
                return;
            } 

            pos.x -= offset;
        }
        else // if (pos.x < transform.position.x - offset)
        {
            pos.x += offset;
        }

        pos.x = Mathf.Clamp(pos.x, minCameraX, maxCameraX);

        if (limitedMovement)
        {
            float minLimited = minPlayerX + cameraDistanceX;
            float maxLimited = maxPlayerX - cameraDistanceX;

            pos.x = Mathf.Clamp(pos.x, Mathf.Min(minLimited, pos.x), maxPlayerX);
        }
        else
        {

        }

        pos.y = transform.position.y;
        pos.z = transform.position.z;

        transform.position = Vector3.Lerp(transform.position, pos, smothSpeed * Time.deltaTime);
    }

    public void SetPlayerBounds(float minPlayerX, float maxPlayerX)
    {
        this.minPlayerX = minPlayerX;
        this.maxPlayerX = maxPlayerX;
        limitedMovement = true;
    }

    public void FreePlayerBounds()
    {
        this.minPlayerX = 0;
        this.maxPlayerX = 0;
        limitedMovement = false;
    }

}
