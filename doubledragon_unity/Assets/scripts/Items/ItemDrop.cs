﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ItemDrop: MonoBehaviour {
    public ItemTypes ItemType = ItemTypes.None;
    public bool IsEquipped = false;
    [HideInInspector]
    public Rigidbody Rigidbody;

    public enum ItemTypes
    {
        None,
        Whip,
        Bat,
        Knife,
        Crate
    }

    public void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
    }
}
