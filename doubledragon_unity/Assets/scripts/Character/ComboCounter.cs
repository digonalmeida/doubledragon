﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ComboCounter{
    
    float _Timeout = 1;
    float _CurrentTimeout = 0;
    [SerializeField]
    int _CurrentCombo = 0;

    public ComboCounter(float timeout)
    {
        _Timeout = timeout;
    }

    public int CurrentCombo {
        get
        {
            return _CurrentCombo;
        }
        set
        {
            _CurrentCombo = value;
        }
    }

    public void addCombo()
    {
        CurrentCombo++;
        _CurrentTimeout = _Timeout;
    }

    public void Update()
    {
        if(_CurrentTimeout > 0)
        {
            _CurrentTimeout -= Time.deltaTime;
            if(_CurrentTimeout <= 0)
            {
                Reset();
            }
        }
    }

    public void Reset()
    {
        _CurrentTimeout = 0;
        CurrentCombo = 0;
    }
}
