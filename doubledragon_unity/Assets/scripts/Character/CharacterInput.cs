﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInput {
    public Vector3 MovementDirection = new Vector3();
    public Vector3 LookDirection = new Vector3(0, 0, 0);
    public bool Jump = false;
    public bool Punch = false;
    public bool Kick = false;
    public bool getHit = false;
}
