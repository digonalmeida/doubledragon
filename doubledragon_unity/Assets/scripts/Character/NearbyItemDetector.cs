﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NearbyItemDetector : MonoBehaviour {
    public List<ItemDrop.ItemTypes> PickableItems = new List<ItemDrop.ItemTypes>();
    private Character _Agent;
    private List<ItemDrop> _NearbyItems = new List<ItemDrop>();
    private List<ItemDrop> _NearbyPickableItems = new List<ItemDrop>();

    void Awake()
    {
        _Agent = transform.parent.GetComponent<Character>();
        if(_Agent == null)
        {
            Debug.LogError("NearbyItemDetector precisa de um Character como pai");
        }
    }
    public void UpdatePickableItems()
    {
        _NearbyPickableItems.Clear();
        foreach(var item in _NearbyItems)
        {
            if(item.IsEquipped)
            {
                continue;
            }
            if(!PickableItems.Contains(item.ItemType))
            {
                continue;
            }
            _NearbyPickableItems.Add(item);
        }
    }
    public void UpdateAgentNearbyItem()
    {
        UpdatePickableItems();
        if (_NearbyPickableItems.Count > 0)
        {
            _Agent.NearbyItem = _NearbyPickableItems[0];
        }
        else
        {
            _Agent.NearbyItem = null;
        }
    }

    void Update()
    {
        UpdateAgentNearbyItem();
    }

    void OnTriggerEnter(Collider other)
    {
        ItemDrop itemDrop = other.GetComponent<ItemDrop>();
        if (itemDrop == null)
        {
            return;
        }

        if (!_NearbyItems.Contains(itemDrop))
        {
            _NearbyItems.Add(itemDrop);
        }
        UpdateAgentNearbyItem();
    }


    void OnTriggerExit(Collider other)
    {
        ItemDrop itemDrop = other.GetComponent<ItemDrop>();
        if (itemDrop == null)
        {
            return;
        }

        if (_NearbyItems.Contains(itemDrop))
        {
            _NearbyItems.Remove(itemDrop);
        }
        UpdateAgentNearbyItem();
    }
}
