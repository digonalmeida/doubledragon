﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
using CharacterNs;

public class CharacterStatesGui : MonoBehaviour {
    public Character Character;
    List<GuiState> _GuiStates = new List<GuiState>();
    GUIStyle style = new GUIStyle();

    void OnGUI()
    {
        foreach(GuiState guiState in _GuiStates)
        {
            if (GUILayout.Button(guiState.Name, style))
            {
                Character.StateMachine.SetState(guiState.State);
            }
        }
    }

	void Start () {
        Add("idle", new IdleState());
        Add("walking", new WalkingState());
        Add("jumping", new JumpingState());
        Add("jump Kick", new JumpKickState());
        Add("punch combo", new PunchComboState());
        Add("punch stun combo", new PunchStunComboState());
        Add("hard punch stun", new AirStunFallingState());
        Add("air kick stun", new AirStunFallingState());
        Add("catching", new CatchingState());
        Add("catch stun", new CatchStunState());
        Add("catch throw", new CatchThrowState());
        Add("catch throw stun", new CatchThrowStunState());
        Add("catch throw stun fall", new ThrowFallingState());
        Add("getting up", new GettingUpState());

        Add("death", new DeathState());
    }

    private void Add(string name, State<Character> state)
    {
        _GuiStates.Add(new GuiState(name, state));
    }

    class GuiState
    {
        public string Name;
        public State<Character> State;
        public GuiState(string name, State<Character> state)
        {
            Name = name;
            State = state;
        }
    }
}
