﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
using CharacterNs;
using TriggerEvents = Character.TriggerEvents;

[RequireComponent(typeof(Character))]
public class LindaStateMachine : MonoBehaviour {

    private Character _Character;

    #region private State Machine

    private IdleState _Idle = new IdleState();
    private WalkingState _Walking = new WalkingState();
    private AirStunFallingState _JumpKickStunned = new AirStunFallingState();
    private AnimationComboState _PunchComboState;
    private WhipAttackState _WhipAttackState = new WhipAttackState();
    private PunchStunComboState _PunchComboStunned = new PunchStunComboState(false);
    private AirStunFallingState _HardPunchStunned = new AirStunFallingState();
    private PunchFallState _PunchFalling = new PunchFallState();
    private GettingUpState _GettingUp = new GettingUpState();
    private DeathState _Death = new DeathState();
    private PickUpState _PickingUp = new PickUpState();

    #endregion

    #region Unity Methods

    void Awake()
    {
        _Character = GetComponent<Character>();
    }

    void Start()
    {
        SetupStateMachine();
    }
    #endregion

    void SetupStateMachine()
    {
        _PunchComboState = new AnimationComboState(_Character.HitComboCounter, new string[] { CharacterAnimations.Punch1, CharacterAnimations.Punch2 });


        SetupFreeMovementStates();

        SetupActionStates();

        SetupStunnedStates();

        SetupFallenStates();

        _Character.StateMachine.SetState(_Idle);
    }

    //can attack, jump, catch, and be stunned
    void SetupFreeMovementStates()
    {
        List<State<Character>> freeStates = new List<State<Character>>();

        freeStates.Add(_Idle);
        _Idle.AddCondition(_Character.IsInputDirectionNotZero, _Walking);

        freeStates.Add(_Walking);
        _Walking.AddCondition(_Character.IsInputDirectionZero, _Idle);

        foreach (State<Character> state in freeStates)
        {
            state.AddCondition(_Character.IsPickingUpWhip, _PickingUp);
            state.AddCondition(_Character.IsAttackingWithWhip, _WhipAttackState);
            state.AddTrigger((int)TriggerEvents.PunchHit, _PunchComboStunned);
            state.AddTrigger((int)TriggerEvents.HardPunchHit, _HardPunchStunned);
            state.AddTrigger((int)TriggerEvents.AirKickHit, _JumpKickStunned);
            state.AddCondition(_Character.IsPunching, _PunchComboState);
            state.AddCondition(_Character.CheckIsDead, _HardPunchStunned);
        }
    }
    
    //can be stunned. Go back to idle after animation finished
    void SetupActionStates()
    {
        List<State<Character>> actionStates = new List<State<Character>>();

        actionStates.Add(_PunchComboState);
        //_PunchComboState.AddTransition((int)TriggerEvents.AnimationEnded, _Character.IsPunching, _PunchComboState);
        actionStates.Add(_WhipAttackState);
        actionStates.Add(_PickingUp);
        actionStates.Add(_GettingUp);


        foreach (State<Character> state in actionStates)
        {
            state.AddTrigger((int)TriggerEvents.PunchHit, _PunchComboStunned);
            state.AddTrigger((int)TriggerEvents.HardPunchHit, _HardPunchStunned);
            state.AddTrigger((int)TriggerEvents.AirKickHit, _JumpKickStunned);
            state.AddCondition(_Character.CheckIsDead, _PunchFalling);
            state.AddTrigger((int)TriggerEvents.AnimationEnded, _Idle);
        }
    }

    //can be stunned again, and go back to idle on animation finished
    void SetupStunnedStates()
    {
        List<State<Character>> stunStates = new List<State<Character>>();

        stunStates.Add(_PunchComboStunned);
        _PunchComboStunned.AddCondition(_Character.CheckIsDead, _HardPunchStunned);
        

        foreach (State<Character> state in stunStates)
        {
            state.AddTrigger((int)TriggerEvents.PunchHit, _PunchComboStunned);
            state.AddTrigger((int)TriggerEvents.HardPunchHit, _HardPunchStunned);
            state.AddTrigger((int)TriggerEvents.AirKickHit, _JumpKickStunned);
            state.AddTrigger((int)TriggerEvents.AnimationEnded, _Idle);
        }
    }

    //can get up or die
    void SetupFallenStates()
    {
        List<State<Character>> fallenStates = new List<State<Character>>();

        fallenStates.Add(_HardPunchStunned);
        fallenStates.Add(_JumpKickStunned);
        fallenStates.Add(_PunchFalling);

        foreach (State<Character> state in fallenStates)
        {
            state.AddTransition((int)TriggerEvents.AnimationEnded, _Character.CheckIsDead, _Death);
            state.AddTransition((int)TriggerEvents.AnimationEnded, () => !_Character.CheckIsDead(), _GettingUp);
        }
    }

    
}
