﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
using CharacterNs;
using TriggerEvents = Character.TriggerEvents;

[RequireComponent(typeof(Character))]
public class WilliamsStateMachine : MonoBehaviour {

    private Character _Character;

    #region private State Machine

    private IdleState _Idle = new IdleState();
    private WalkingState _Walking = new WalkingState();
    private JumpingState _Jumping = new JumpingState();
    private JumpKickState _JumpKicking = new JumpKickState();
    private AirStunFallingState _JumpKickStunned = new AirStunFallingState();
    private AnimationComboState _PunchComboState;
    private PunchStunComboState _PunchComboStunned = new PunchStunComboState();
    private AirStunFallingState _HardPunchStunned = new AirStunFallingState();
    private PunchFallState _PunchFalling = new PunchFallState();
    private GettingUpState _GettingUp = new GettingUpState();
    private DeathState _Death = new DeathState();
    private CatchStunState _CatchStunned = new CatchStunState();
    private CatchThrowState _CatchThrowing = new CatchThrowState();
    private CatchThrowStunState _CatchThrowStunned = new CatchThrowStunState();
    private ThrowFallingState _ThrowFalling = new ThrowFallingState();

    #endregion

    #region Unity Methods

    void Awake()
    {
        
        _Character = GetComponent<Character>();
    }

    void Start()
    {
        SetupStateMachine();
    }
    #endregion

    void SetupStateMachine()
    {
        _PunchComboState = new AnimationComboState(_Character.HitComboCounter, new string[] { CharacterAnimations.Punch1, CharacterAnimations.Punch2 });


        SetupFreeMovementStates();

        SetupActionStates();

        SetupStunnedStates();

        SetupFallenStates();

        _Character.StateMachine.SetState(_Idle);
    }

    //can attack, jump, catch, and be stunned
    void SetupFreeMovementStates()
    {
        List<State<Character>> freeStates = new List<State<Character>>();

        freeStates.Add(_Idle);
        _Idle.AddCondition(_Character.IsInputDirectionNotZero, _Walking);

        freeStates.Add(_Walking);
        _Walking.AddCondition(_Character.IsInputDirectionZero, _Idle);

        foreach (State<Character> state in freeStates)
        {
            state.AddTrigger((int)TriggerEvents.PunchHit, _PunchComboStunned);
            state.AddTrigger((int)TriggerEvents.HardPunchHit, _HardPunchStunned);
            state.AddTrigger((int)TriggerEvents.AirKickHit, _JumpKickStunned);
            state.AddTrigger((int)TriggerEvents.Catched, _CatchStunned);
            state.AddCondition(_Character.IsPunching, _PunchComboState);
            state.AddCondition(_Character.IsJumping, _Jumping);
            state.AddCondition(_Character.CheckIsDead, _PunchFalling);
            state.AddTrigger((int)TriggerEvents.CatchThrown, _CatchThrowStunned);
        }
    }
    
    //can be stunned. Go back to idle after animation finished
    void SetupActionStates()
    {
        List<State<Character>> actionStates = new List<State<Character>>();

        actionStates.Add(_Jumping);
        _Jumping.AddCondition(_Character.IsAttacking, _JumpKicking);
        _Jumping.AddCondition(_Character.CheckIsGrounded, _Idle);

        actionStates.Add(_JumpKicking);
        _JumpKicking.AddCondition(_Character.CheckIsGrounded, _Idle);

        actionStates.Add(_PunchComboState);

        actionStates.Add(_GettingUp);

        actionStates.Add(_CatchThrowing);

        foreach (State<Character> state in actionStates)
        {
            state.AddTrigger((int)TriggerEvents.PunchHit, _PunchComboStunned);
            state.AddTrigger((int)TriggerEvents.HardPunchHit, _HardPunchStunned);
            state.AddTrigger((int)TriggerEvents.AirKickHit, _JumpKickStunned);
            state.AddTrigger((int)TriggerEvents.Catched, _CatchStunned);
            state.AddCondition(_Character.CheckIsDead, _PunchFalling);
            state.AddTrigger((int)TriggerEvents.AnimationEnded, _Idle);
            state.AddTrigger((int)TriggerEvents.CatchThrown, _CatchThrowStunned);

        }
    }

    //can be stunned again, and go back to idle on animation finished
    void SetupStunnedStates()
    {
        List<State<Character>> stunStates = new List<State<Character>>();

        stunStates.Add(_PunchComboStunned);
        _PunchComboStunned.AddCondition(_Character.CheckIsDead, _PunchFalling);

        stunStates.Add(_CatchStunned);
        _CatchStunned.AddTrigger((int)TriggerEvents.CatchThrown, _CatchThrowStunned);
        _CatchStunned.AddTrigger((int)TriggerEvents.ReleaseCatch, _Idle);

        stunStates.Add(_CatchThrowStunned);
        _CatchThrowStunned.AddTrigger((int)TriggerEvents.AnimationEnded, _ThrowFalling);

        foreach (State<Character> state in stunStates)
        {
            state.AddTrigger((int)TriggerEvents.Catched, _CatchStunned);
            state.AddTrigger((int)TriggerEvents.PunchHit, _PunchComboStunned);
            state.AddTrigger((int)TriggerEvents.HardPunchHit, _HardPunchStunned);
            state.AddTrigger((int)TriggerEvents.AirKickHit, _JumpKickStunned);
            state.AddTrigger((int)TriggerEvents.AnimationEnded, _Idle);
            state.AddTrigger((int)TriggerEvents.CatchThrown, _CatchThrowStunned);
        }
    }

    //can get up or die
    void SetupFallenStates()
    {
        List<State<Character>> fallenStates = new List<State<Character>>();

        fallenStates.Add(_HardPunchStunned);
        fallenStates.Add(_JumpKickStunned);
        fallenStates.Add(_PunchFalling);
        fallenStates.Add(_ThrowFalling);

        foreach (State<Character> state in fallenStates)
        {
            state.AddTransition((int)TriggerEvents.AnimationEnded, _Character.CheckIsDead, _Death);
            state.AddTransition((int)TriggerEvents.AnimationEnded, () => !_Character.CheckIsDead(), _GettingUp);
        }
    }

    
}
