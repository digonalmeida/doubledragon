﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;

public class Character : MonoBehaviour {
    private const float _ClampZMin = -0.45f;
    private const float _ClampZMax = 0.3f;

    #region Components 

    CharacterSensor _NearbyCharacterSensor = new CharacterSensor();
    ItemSensor _NearbyItemSensor = new ItemSensor();

    [HideInInspector]
    public CharacterSprite Sprite;

    [HideInInspector]
    public Rigidbody Rigidbody;

    [HideInInspector]
    public CharacterInput Input = new CharacterInput();

    [HideInInspector]
    private Collider _Collider;
    
    public Dictionary<ItemDrop.ItemTypes, CharacterItemSprite> Items = new Dictionary<ItemDrop.ItemTypes, CharacterItemSprite>();

    #endregion

    #region Character stats

    public float Speed = 1;
    public int MaxHp = 5;
    public int Hp = 5;
    public int Lifes = 0;

    public float AirStunForce = 2.4f;
    public float ThrowForce = 3.2f;
    public Vector3 ThrowDeltaPosition = new Vector3(.1f, .4f, 0);
    public float HorizontalJumpForce = 0.3f;
    public float VerticalJumpForce = 2.4f;

    public List<ItemDrop.ItemTypes> PickableObjects = new List<ItemDrop.ItemTypes>();

    #endregion

    #region State fields

    private ItemDrop ItemDrop = null;
    [HideInInspector]
    public Vector3 HitterDirection = Vector3.right;

    [HideInInspector]
    public Vector3 ThrowDirection = Vector3.up;

    [HideInInspector]
    public bool CanBeCatched = false;

    [HideInInspector]
    public Character TargetCharacter = null;

    [HideInInspector]
    public Character NearbyCharacter = null;

    [HideInInspector]
    public ItemDrop NearbyItem = null;

    [HideInInspector]
    public bool IsInvulnerable = false;

    [HideInInspector]
    public Vector3 AnimationDirection = new Vector3(1, 0, -1);
    
    [HideInInspector]
    public ComboCounter HitComboCounter = new ComboCounter(0.8f);

    [HideInInspector]
    public ComboCounter StunComboCounter = new ComboCounter(0.8f);

    private Vector3 _FacingDirection = new Vector3();
    private CharacterItemSprite[] _ItemSprites;
    #endregion

    public FiniteStateMachine<Character> StateMachine;
    
    public enum TriggerEvents
    {
        AnimationEnded,
        PunchHit,
        HardPunchHit,
        AirKickHit,
        Catched,
        ReleaseCatch,
        CatchThrown,
        Respawn
    }
    
    public Vector3 FacingDirection
    {
        get
        {
            return _FacingDirection;
        }
        set
        {
            _FacingDirection = value;
            if(Sprite == null)
            {
                return;
            }

            Vector3 scale = Sprite.transform.localScale;
            scale.x = Mathf.Sign(FacingDirection.x) * Mathf.Abs(scale.x);
            Sprite.transform.localScale = scale;
            _NearbyCharacterSensor.Direction.x = FacingDirection.x;
            _NearbyCharacterSensor.Origin.x = FacingDirection.x * _Collider.bounds.extents.x;
        }
    }

    public ItemDrop.ItemTypes ItemState
    {
        get
        {
            if(ItemDrop == null)
            {
                return ItemDrop.ItemTypes.None;
            }
            else
            {
                return ItemDrop.ItemType;
            }
        }
    }

    public ItemDrop.ItemTypes NearbyItemType
    {
        get
        {
            if (NearbyItem == null)
            {
                return ItemDrop.ItemTypes.None;
            }
            else
            {
                return NearbyItem.ItemType;
            }
        }
    }

    #region public state machine conditions

    public bool CheckIsGrounded()
    {
        Ray ray = new Ray(transform.position, Vector3.down);
        float raySize = _Collider.bounds.extents.y + 0.05f;
        int layermask = LayerMask.GetMask("Ground");

        Debug.DrawRay(ray.origin, ray.direction * raySize, Color.red, Time.deltaTime);

        RaycastHit hitInfo;
        if(Physics.Raycast(ray, out hitInfo, raySize,  layermask))
        {
            return true;
        }

        return false;
    }
    
    public bool CanCatchNearbyCharacter()
    {
        if (NearbyCharacter == null)
        {
            return false;
        }

        if (NearbyCharacter.CanBeCatched)
        {
            return true;
        }

        return false;
    }
   
    public bool IsWalkingAwayFromTarget()
    {
        if(TargetCharacter == null)
        {
            return true;
        }
        if(Input.MovementDirection.sqrMagnitude > 0)
        {
            if (Mathf.Sign(FacingDirection.x) != Mathf.Sign(Input.MovementDirection.x))
            {
                return true;
            }
        }

        return false;
    }

    public bool CheckIsDead()
    {
        return Hp <= 0;
    }

    public bool IsInputDirectionZero()
    {
        return Input.MovementDirection.sqrMagnitude == 0;
    }

    public bool IsInputDirectionNotZero()
    {
        return !IsInputDirectionZero();
    }

    public bool IsAttacking()
    {
        return IsPunching() || IsKicking();
    }

    public bool IsAttackingWithWhip()
    {
        if(IsAttacking())
        {
            if(ItemState == ItemDrop.ItemTypes.Whip)
            {
                return true;
            }
        }
        return false;
    }
    public bool IsPickingUpItem()
    {
        if (IsAttacking())
        {
            if (NearbyItemType != ItemDrop.ItemTypes.None)
            {
                if (ItemState == ItemDrop.ItemTypes.None)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool IsPickingUpWhip()
    {
        if(IsPickingUpItem())
        {
            if(NearbyItemType == ItemDrop.ItemTypes.Whip)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsPunching()
    {
        return Input.Punch;
    }

    public bool IsKicking()
    {
        return Input.Kick;
    }

    public bool IsJumping()
    {
        return Input.Jump;
    }

    #endregion

    #region Unity Methods

    void Awake()
    {
        Sprite = GetComponentInChildren<CharacterSprite>();
        if(Sprite == null)
        {
            Debug.LogError("Could not find character sprite in character");
        }
        Sprite.Agent = this;

        StateMachine = new FiniteStateMachine<Character>(this);
        _Collider = GetComponent<Collider>();
        Rigidbody = GetComponent<Rigidbody>();
        _ItemSprites = GetComponentsInChildren<CharacterItemSprite>(true);//includeInactive = true

        SetupHitters();
        SetupSensors();
        //sometimes the character starts with an item
        ItemDrop = GetComponentInChildren<ItemDrop>();
        if (ItemDrop != null)
        {
            PickupItem(ItemDrop);
        }
    }

    void SetupSensors()
    {
        _NearbyCharacterSensor.type = CharacterSensor.Type.Sphere;
        _NearbyCharacterSensor.Origin.x = _Collider.bounds.extents.x;
        _NearbyCharacterSensor.Direction = Vector3.right;
        _NearbyCharacterSensor.LayerMask = LayerMask.GetMask("Character");
        _NearbyCharacterSensor.Distance = 0.1f;
        _NearbyCharacterSensor.Ignore.Add(this);

        _NearbyItemSensor.type = ItemSensor.Type.Sphere;
        _NearbyItemSensor.Origin = Vector3.zero;
        _NearbyItemSensor.Origin.y = -_Collider.bounds.extents.y;
        _NearbyItemSensor.Direction = Vector3.down;
        _NearbyItemSensor.LayerMask = LayerMask.GetMask("Character");
        _NearbyItemSensor.Distance = 0.1f;
        _NearbyItemSensor.ItemTypes = PickableObjects;
    }
    
    void SetupHitters()
    {
        var hitters = GetComponentsInChildren<CharacterHitter>(true);//includeInactive = true
        foreach (var hitter in hitters)
        {
            hitter.Agent = this;
        }
    }

    void SetItemSpriteActive(ItemDrop.ItemTypes activeType)
    {
        foreach(var itemSprite in _ItemSprites)
        {
            itemSprite.gameObject.SetActive(itemSprite.ItemType == activeType); 
        }
    }

    void Update()
    {
        if(Input.getHit)
        {
            Hurt(null);
        }

        HitComboCounter.Update();
        StunComboCounter.Update();

        UpdateSensors();

        StateMachine.Update();
        ClampZ();

        
    }

    void OnDrawGizmos()
    {
        _NearbyCharacterSensor.DrawGizmos();
        _NearbyItemSensor.DrawGizmos();
    }
    #endregion

    void UpdateSensors()
    {
        NearbyCharacter = _NearbyCharacterSensor.SenseFirst(transform.position);
        NearbyItem = _NearbyItemSensor.SenseFirst(transform.position);
    }
    public void Move()
    {
        transform.position += Input.MovementDirection.normalized * Speed * Time.deltaTime;
        AnimationDirection.x = Mathf.Sign(Input.MovementDirection.normalized.x);
        AnimationDirection.z = Input.MovementDirection.normalized.z > 0 ? 1 : -1;
        ClampZ();
    }

    public void Respawn()
    {
        Hp = MaxHp;
        StateMachine.TriggerEvent((int)TriggerEvents.Respawn);
    }

    #region Event Listener Methods

    public void OnAnimationEnded()
    {
        StateMachine.TriggerEvent((int)TriggerEvents.AnimationEnded);
    }
    
    public void OnHit()
    {
        HitComboCounter.addCombo();
    }

    public void OnCatched(Character catcher)
    {
        TargetCharacter = catcher;
        StateMachine.TriggerEvent((int)TriggerEvents.Catched);
    }

    public void OnCatchThrown(Character catcher)
    {
        TargetCharacter = catcher;
        Hp -= 2;
        StateMachine.TriggerEvent((int)TriggerEvents.CatchThrown);
    }

    public void OnReleaseCatch()
    {
        StateMachine.TriggerEvent((int)TriggerEvents.ReleaseCatch);
    }
    
    public void Hurt(CharacterHitter hitter)
    {

        CharacterHitter.Types type = CharacterHitter.Types.Punch;
        if (hitter == null)
        {
            return;
        }

        type = hitter.type;
        HitterDirection = hitter.Agent.transform.position - transform.position;
        HitterDirection.y = 0;
        HitterDirection.Normalize();

        switch (type)
        {
            case CharacterHitter.Types.Punch:
                Hp -= 1;
                ThrowDirection = Vector3.up;
                if (StunComboCounter.CurrentCombo >= 2)
                {
                    StateMachine.TriggerEvent((int)TriggerEvents.HardPunchHit);
                }
                else
                {
                    StateMachine.TriggerEvent((int)TriggerEvents.PunchHit);
                }
                break;

            case CharacterHitter.Types.HardPunch:
                Hp -= 2;
                ThrowDirection = Vector3.up;
                StateMachine.TriggerEvent((int)TriggerEvents.HardPunchHit);
                break;

            case CharacterHitter.Types.AirKick:
                Hp -= 2;
                ThrowDirection = Vector3.up;
                ThrowDirection = transform.position - hitter.Agent.transform.position;
                ThrowDirection.y = 1;
                ThrowDirection.Normalize();
                //ThrowDirection.y = 1;
                StateMachine.TriggerEvent((int)TriggerEvents.AirKickHit);
                break;
            default:
                break;
        }

    }


    public void PickupItem(ItemDrop itemDrop)
    {
        if(ItemDrop != null)
        {
            ReleaseItem();
        }
        
        ItemDrop = itemDrop;
        ItemDrop.IsEquipped = true;
        ItemDrop.gameObject.SetActive(false);
        SetItemSpriteActive(ItemDrop.ItemType);
    }

    public void ReleaseItem()
    {
        if (ItemDrop != null)
        {
            ItemDrop.transform.position = transform.position;
            ItemDrop.transform.parent = null;
            ItemDrop.IsEquipped = false;
            SetItemSpriteActive(ItemDrop.ItemTypes.None);
            ItemDrop.gameObject.SetActive(true);
            ItemDrop = null;
            
        }
    }

    #endregion

    private void ClampZ()
    {
        Vector3 pos = transform.position;
        pos.z = Mathf.Clamp(pos.z, _ClampZMin, _ClampZMax);
        transform.position = pos;
    }
}