﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterResetTimer : MonoBehaviour {

    private Character boss;

    private void Start ()
    {
        boss = GetComponent<Character>();
	}
	
	private void Update ()
    {
        if (boss.Hp <= 0f)
        {
            Timer.Instance.Reset();
            Destroy(this);
        }
	}
}
