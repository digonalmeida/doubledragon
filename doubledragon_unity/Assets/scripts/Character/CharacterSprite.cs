﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
public class CharacterSprite : MonoBehaviour {

    [HideInInspector]
    public Character Agent;

    [HideInInspector]
    public Animator Animator;

    [HideInInspector]
    public SpriteRenderer SpriteRenderer;

    void Awake()
    {
        Animator = GetComponent<Animator>();
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void AnimationEnded()
    {
        if(Agent != null)
        {
            Agent.OnAnimationEnded();
        }
    }
    
}
