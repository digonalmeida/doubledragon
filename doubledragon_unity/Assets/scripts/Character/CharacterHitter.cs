﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterHitter : MonoBehaviour {
    

    public Types type;
    private bool _HitSomething = false;
    private List<Character> _HurtTargets = new List<Character>();

    [HideInInspector]
    public Character Agent;

    public enum Types
    {
        Punch,
        HardPunch,
        AirKick
    }

    void Awake()
    {
        _HitSomething = false;
    }

    void OnDisable()
    {
        _HitSomething = false;
        _HurtTargets.Clear();
    }

    void OnTriggerEnter(Collider other)
    {
        var character = other.GetComponent<Character>();
        if(character == null)
        {
            return;
        }
        if(character == Agent)
        {
            return;
        }

        if(_HurtTargets.Contains(character))
        {
            return;
        }
        
        character.Hurt(this);

        _HurtTargets.Add(character);

        if(!_HitSomething)
        {
            _HitSomething = true;
            if(Agent != null)
            {
                Agent.OnHit();
            }
        }
    }
}
