﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {
    public Character Agent;

    void Awake()
    {
        LevelManager.Instance.Player1 = Agent;
        GameManager.Instance.OnStart1KeyPressed += OnStart1KeyPressed;
    }

    void Destroy()
    {
        GameManager.Instance.OnStart1KeyPressed -= OnStart1KeyPressed;
    }

    void OnStart1KeyPressed()
    {
        if(GameManager.Instance.credits <= 0)
        {
            return;
        }

        if(Agent.Hp > 0)
        {
            return;
        }

        if(Agent.Lifes > 0)
        {
            return;
        }

        GameManager.Instance.credits--;
        Agent.Respawn();
    }

    void Update()
    {
        Agent.Input.Jump = Input.GetButton("Jump");
        Agent.Input.Punch = Input.GetButton("Fire1");
        Agent.Input.Kick = Input.GetButton("Fire2");
        Agent.Input.MovementDirection.x = Input.GetAxis("Horizontal");
        Agent.Input.MovementDirection.z = Input.GetAxis("Vertical");
        Agent.Input.getHit = Input.GetKeyDown(KeyCode.X);

        if(Agent.Input.MovementDirection.sqrMagnitude > 0)
        {
            Agent.Input.LookDirection.x = (Mathf.Sign(Agent.Input.MovementDirection.x));
            Agent.Input.LookDirection.z = (Mathf.Sign(Agent.Input.MovementDirection.z));
        }
    }
}
