﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterNs
{
    public class CharacterAnimations
    {
        public const string IdleUp = "idleUp";
        public const string IdleDown = "idleDown";
        public const string WalkingUp = "walkingUp";
        public const string WalkingDown = "walkingDown";
        public const string AirKick = "airKick";
        public const string Jumping = "jumping";

        public const string AirStunFallUp = "airStunFallUp";
        public const string AirStunFallDown = "airStunFallDown";
        public const string AirStunFallGround = "airStunFallGround";

        public const string Punch1 = "punch1";
        public const string Punch2 = "punch2";
        public const string Punch3 = "punch3";

        public const string PunchStun1 = "punchStun1";
        public const string PunchStun2 = "punchStun2";
        public const string PunchStunFall = "punchStunFall";

        public const string Catching = "catching";
        public const string CatchThrowing = "catchThrow";

        public const string CatchStun = "catchStun";
        public const string CatchThrowStun = "catchThrowStun";
        public const string ThrowStunFall = "throwStunFall";
        public const string ThrowStunFallGround = "throwStunFallGround";

        public const string Blink = "deadBlink";
        public const string NoBlink = "noBlink";

        public const string GettingUp = "gettingUp";

        public const string PickingUp = "pickingUp";
        public const string WhipAttack = "whipAttack";

        public const int BaseLayer = 0;
        public const int BlinkLayer = 1;
    }
}

