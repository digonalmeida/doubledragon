﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterContinue : MonoBehaviour {

    private Character _player;
    private int _playerMaxHealth;

    private void Start ()
    {
        if (LevelManager.Instance == null)
        {
            Destroy(this);
            return;
        }
        
        _player = GetComponent<Character>();
        _playerMaxHealth = _player.Hp;
    }
    
    private void Update ()
    {
        if (LevelManager.Instance.EndingGame)
        {
            if (GameManager.Instance.start1KeyPressed && GameManager.Instance.credits > 0)
            {
                GameManager.Instance.credits -= 1;

                _player.Hp = _playerMaxHealth;
                _player.Lifes = 1;
                _player.Respawn();
            }            
        }
    }
}
