﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class CatchingState : State<Character>
    {
        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.Catching);
            Agent.TargetCharacter = Agent.NearbyCharacter;
            Agent.FacingDirection = Agent.Input.LookDirection;
            Agent.TargetCharacter.OnCatched(Agent);
        }

        public override void Exit()
        {
            Agent.TargetCharacter.OnReleaseCatch();
            base.Exit();
        }
    }
}