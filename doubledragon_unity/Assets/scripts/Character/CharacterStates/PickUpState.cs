﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class PickUpState : State<Character>
    {
        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.PickingUp);
            Agent.PickupItem(Agent.NearbyItem);
        }
    }
}