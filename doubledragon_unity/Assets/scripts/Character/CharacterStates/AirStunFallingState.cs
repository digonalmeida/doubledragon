﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;

namespace CharacterNs
{
    public class AirStunFallingState : State<Character>
    {

        public override void Enter()
        {
            base.Enter();
            
            Agent.IsInvulnerable = true;
            Agent.Rigidbody.velocity = Vector3.zero;
            Agent.Rigidbody.AddForce(Agent.ThrowDirection * Agent.AirStunForce, ForceMode.VelocityChange);
            Agent.ReleaseItem();
        }

        public override void Update()
        {
            base.Update();
            
            if (Agent.Rigidbody.velocity.y > 0.5f)
            {
                
                Agent.Sprite.Animator.Play(CharacterAnimations.AirStunFallUp);
            }
            else if (Agent.CheckIsGrounded())
            {
                Agent.Sprite.Animator.Play(CharacterAnimations.AirStunFallGround);
            }
            else
            {
                Agent.Sprite.Animator.Play(CharacterAnimations.AirStunFallDown);
            }
        }

        public override void Exit()
        {
            base.Exit();
            Agent.IsInvulnerable = false;
        }
    }
}

