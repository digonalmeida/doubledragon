﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class GettingUpState : State<Character>
    {
        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.GettingUp);
            Agent.IsInvulnerable = true;
        }

        public override void Exit()
        {
            base.Exit();
            Agent.IsInvulnerable = false;
        }
    }
}