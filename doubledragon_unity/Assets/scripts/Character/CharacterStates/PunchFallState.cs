﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;

namespace CharacterNs
{
    public class PunchFallState : State<Character>
    {
        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.PunchStunFall);
            Agent.IsInvulnerable = true;
            Agent.ReleaseItem();
        }

        public override void Exit()
        {
            base.Exit();
            Agent.IsInvulnerable = false;
        }
    }
}