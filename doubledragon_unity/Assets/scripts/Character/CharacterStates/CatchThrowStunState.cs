﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class CatchThrowStunState : State<Character>
    {
        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.CatchThrowStun);
            Agent.IsInvulnerable = true;
            Agent.transform.position = Agent.TargetCharacter.transform.position;
            Agent.transform.position += new Vector3(0, 0, 0.1f);

            Agent.FacingDirection = -Agent.TargetCharacter.FacingDirection;
            Agent.ThrowDirection.x = Mathf.Sign(Agent.FacingDirection.x);
            Agent.ThrowDirection.y = 1;
            Agent.ThrowDirection.Normalize();
            
        }

        public override void Exit()
        {
            base.Exit();
            Agent.IsInvulnerable = false;
        }
    }
}