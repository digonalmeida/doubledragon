﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class IdleState : State<Character>
    {
        public override void Update()
        {
            base.Update();

            if(Agent.Input.LookDirection != Vector3.zero)
            {
                Agent.FacingDirection = Agent.Input.LookDirection;
            }
            
            if (Agent.FacingDirection.y > 0)
            {
                Agent.Sprite.Animator.Play(CharacterAnimations.IdleUp);
            }
            else
            {
                Agent.Sprite.Animator.Play(CharacterAnimations.IdleDown);
            }
        }
    }
}