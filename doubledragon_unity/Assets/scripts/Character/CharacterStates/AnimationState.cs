﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class AnimationState : State<Character>
    {
        private string _AnimationName = CharacterAnimations.IdleDown;

        public AnimationState()
        {
        }

        public AnimationState(string animationName)
        {
            _AnimationName = animationName;
        }


        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(_AnimationName);
        }

        public override void Exit()
        {
            base.Exit();
        }
    }
}