﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class WhipAttackState : State<Character>
    {
        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.WhipAttack);
        }
    }
}