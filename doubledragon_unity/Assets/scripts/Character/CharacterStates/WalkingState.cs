﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class WalkingState : State<Character>
    {
        public override void Update()
        {
            base.Update();

            Agent.FacingDirection = Agent.Input.LookDirection;

            if (Agent.FacingDirection.z > 0)
            {
                Agent.Sprite.Animator.Play(CharacterAnimations.WalkingUp);
            }
            else
            {
                Agent.Sprite.Animator.Play(CharacterAnimations.WalkingDown);
            }

            Agent.Move();
        }
    }
}