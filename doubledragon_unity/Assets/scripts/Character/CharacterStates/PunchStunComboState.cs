﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;

namespace CharacterNs
{
    public class PunchStunComboState : AnimationComboState
    {
        bool _CanBeCatched = true;
        public PunchStunComboState(bool canBeCatched = true)
        {
            _AnimationNames = new string[] { CharacterAnimations.PunchStun1, CharacterAnimations.PunchStun2};
            _CanBeCatched = canBeCatched;
            _AddToCombo = true;
        }
        public override void Enter()
        {
            Agent.FacingDirection = Agent.HitterDirection;
            _ComboCounter = Agent.StunComboCounter;
            base.Enter();
            if(_CanBeCatched)
            {
                Agent.CanBeCatched = true;
            }
        }

        public override void Exit()
        {
            if(_CanBeCatched)
            {
                Agent.CanBeCatched = false;
            }
        }
    }
}
