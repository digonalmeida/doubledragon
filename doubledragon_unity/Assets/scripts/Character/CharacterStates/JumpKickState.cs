﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class JumpKickState : State<Character>
    {
    
        public override void Enter()
        {
            base.Enter();
            float time = Agent.Sprite.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            Agent.Sprite.Animator.Play(CharacterAnimations.AirKick, CharacterAnimations.BaseLayer, time);

        }
    }
}