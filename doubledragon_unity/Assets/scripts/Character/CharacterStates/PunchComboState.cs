﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;

namespace CharacterNs
{
    public class PunchComboState : AnimationComboState
    {
        public PunchComboState()
        {
            _AnimationNames = new string[] { CharacterAnimations.Punch1, CharacterAnimations.Punch2, CharacterAnimations.Punch3};
            _AddToCombo = false;
        }

        public override void Enter()
        {
            _ComboCounter = Agent.HitComboCounter;
            base.Enter();
        }
    }
}