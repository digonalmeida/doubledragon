﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class JumpingState : State<Character>
    {
        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.Jumping);

            float horizontalDirection = Agent.Input.MovementDirection.x;
            if(horizontalDirection != 0)
            {
                horizontalDirection = Mathf.Sign(horizontalDirection);
            }

            float verticalDirection = 1;

            Vector2 jumpForce = new Vector2(0,0);
            jumpForce.x = horizontalDirection * Agent.HorizontalJumpForce;
            jumpForce.y = verticalDirection * Agent.VerticalJumpForce;
           
            Agent.Rigidbody.velocity = Vector3.zero;
            Agent.Rigidbody.AddForce(jumpForce, ForceMode.VelocityChange);
        }

    }
}