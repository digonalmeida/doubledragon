﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class RespawnState : State<Character>
    {

        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.Blink, CharacterAnimations.BlinkLayer);
            Agent.IsInvulnerable = true;
            Agent.Hp = Agent.MaxHp;
            Agent.Lifes--;
            Agent.OnAnimationEnded();
        }

        public override void Update()
        {
            base.Update();
            
        }

        public override void Exit()
        {
            base.Exit();
            Agent.Sprite.Animator.Play(CharacterAnimations.NoBlink, CharacterAnimations.BlinkLayer);
            Agent.IsInvulnerable = false;
        }
    }
}