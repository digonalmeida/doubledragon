﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class CatchStunState : State<Character>
    {
        public override void Enter()
        {

            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.CatchStun);
            Agent.IsInvulnerable = true;
            Agent.transform.position = Agent.TargetCharacter.transform.position;
            Agent.transform.position += new Vector3(0, 0, 0.1f);
            Agent.FacingDirection = -Agent.TargetCharacter.FacingDirection;
            Agent.ReleaseItem();
        }

        public override void Exit()
        {
            if (Agent.TargetCharacter != null)
            {
                Agent.TargetCharacter.OnReleaseCatch();
            }

            base.Exit();
            Agent.IsInvulnerable = false;
        }
    }
}