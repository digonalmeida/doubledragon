﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class DeathState : State<Character>
    {

        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.Blink, CharacterAnimations.BlinkLayer);
            Agent.IsInvulnerable = true;
        }

        public override void Exit()
        {
            base.Exit();
            Agent.Sprite.Animator.Play(CharacterAnimations.NoBlink, CharacterAnimations.BlinkLayer);
            Agent.IsInvulnerable = false;
        }
    }

}