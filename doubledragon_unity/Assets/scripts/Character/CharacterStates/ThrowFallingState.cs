﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;

namespace CharacterNs
{
    public class ThrowFallingState : State<Character>
    {
        public override void Enter()
        {
            base.Enter();
            Agent.IsInvulnerable = true;

            Vector3 deltaPosition = Agent.ThrowDeltaPosition;
            deltaPosition.x *= Mathf.Sign(Agent.ThrowDirection.x);
            Agent.transform.position += deltaPosition;

            Agent.Rigidbody.velocity = Vector3.zero;
            Agent.Rigidbody.AddForce(Agent.ThrowDirection * Agent.AirStunForce, ForceMode.VelocityChange);
            Agent.ReleaseItem();
        }

        public override void Update()
        {
            base.Update();
            
            if (Agent.CheckIsGrounded())
            {
                Agent.Sprite.Animator.Play(CharacterAnimations.ThrowStunFallGround);
            }
            else
            {
                Agent.Sprite.Animator.Play(CharacterAnimations.ThrowStunFall);
            }
        }

        public override void Exit()
        {
            base.Exit();
            Agent.IsInvulnerable = false;
        }
    }
}