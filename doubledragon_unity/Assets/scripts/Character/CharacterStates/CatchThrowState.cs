﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class CatchThrowState : State<Character>
    {

        public override void Enter()
        {
            base.Enter();
            Agent.Sprite.Animator.Play(CharacterAnimations.CatchThrowing);
            Agent.FacingDirection = Agent.Input.LookDirection;
            Agent.TargetCharacter.OnCatchThrown(Agent);
        }

        public override void Exit()
        {
            Agent.TargetCharacter.OnReleaseCatch();
            base.Exit();
        }
    }
}