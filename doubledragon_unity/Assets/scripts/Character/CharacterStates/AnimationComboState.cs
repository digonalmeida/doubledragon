﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachines;
namespace CharacterNs
{
    public class AnimationComboState : State<Character>
    {
        protected string[] _AnimationNames;
        protected ComboCounter _ComboCounter;
        protected bool _AddToCombo = false;
        public AnimationComboState()
        {

        }
        public AnimationComboState(ComboCounter comboCounter, string[] animationNames)
        {
            _ComboCounter = comboCounter;
            _AnimationNames = animationNames;
        }

        public AnimationComboState(ComboCounter comboCounter, string[] animationNames, bool autoCount)
        {
            _ComboCounter = comboCounter;
            _AnimationNames = animationNames;
            _AddToCombo = autoCount;
        }

        public override void Enter()
        {
            base.Enter();

            int animationId = _ComboCounter.CurrentCombo;
            if (animationId >= _AnimationNames.Length)
            {
                animationId = 0;
                _ComboCounter.Reset();
            }
            if (animationId >= 0)
            {
                Agent.Sprite.Animator.Play(_AnimationNames[animationId]);
            }

            if (_AddToCombo)
            {
                _ComboCounter.addCombo();
            }

        }
    }
}