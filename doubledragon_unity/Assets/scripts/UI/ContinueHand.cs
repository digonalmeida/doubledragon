﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueHand : MonoBehaviour {

    public static ContinueHand Instance { get; private set; }

    public int blinks = 3;
    public float blinkDuration = 1f;

    private Image _hand;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        _hand = GetComponent<Image>();
        _hand.enabled = false;
    }

    public void Show()
    {
        StartCoroutine(BlinkHand());
    }

    IEnumerator BlinkHand()
    {
        for (int i=0; i<blinks; i++)
        {
            _hand.enabled = true;

            yield return new WaitForSeconds(blinkDuration);

            _hand.enabled = false;

            yield return new WaitForSeconds(blinkDuration);
        }
    }

}
