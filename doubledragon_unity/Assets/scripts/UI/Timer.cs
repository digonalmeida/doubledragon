﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public static Timer Instance { get; private set; }

    public float duration = 61f;

    private float _counter;
    private Text _timer;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        _counter = duration;
        _timer = GetComponent<Text>();
    }

    private void Update ()
    {
        if (_counter > 0f)
        {
            _counter -= Time.deltaTime;
            _timer.text = _counter.ToString("00");
        }
        else
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        //
    }

    public void Reset()
    {
        _counter = duration;
    }
}
