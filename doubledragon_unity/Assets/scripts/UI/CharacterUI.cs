﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterUI : MonoBehaviour {

    public Character player;

    public GameObject bar1;
    public GameObject bar2;
    public GameObject bar3;
    public GameObject bar4;
    public GameObject bar5;

    public Text lifes;

    private int _maxHealth;

    private int _currentBar = 5;
    private int _lastBar = 0;

    void Start ()
    {
        if (player == null)
        {
            Destroy(this);
            return;
        }

        _maxHealth = player.Hp;
    }

	void Update ()
    {
        VerifyCharacterHealth();
        UpdateHealthBar();
    }

    void VerifyCharacterHealth()
    {
        int health = (int)(player.Hp * 100f / (float)_maxHealth);
        
        if (health <= 0)
        {
            _currentBar = 0;
        }
        else if (health <= 20)
        {
            _currentBar = 1;
        }
        else if (health <= 40)
        {
            _currentBar = 2;
        }
        else if (health <= 60)
        {
            _currentBar = 3;
        }
        else if (health <= 80)
        {
            _currentBar = 4;
        }
        else
        {
            _currentBar = 5;
        }
    }

    void UpdateHealthBar()
    {
        if (_currentBar != _lastBar)
        {
            _lastBar = _currentBar;

            switch (_currentBar)
            {
                case 5:
                    bar5.SetActive(true);
                    bar4.SetActive(true);
                    bar3.SetActive(true);
                    bar2.SetActive(true);
                    bar1.SetActive(true);
                    break;

                case 4:
                    bar5.SetActive(false);
                    break;

                case 3:
                    bar5.SetActive(false);
                    bar4.SetActive(false);
                    break;

                case 2:
                    bar5.SetActive(false);
                    bar4.SetActive(false);
                    bar3.SetActive(false);
                    break;

                case 1:
                    bar5.SetActive(false);
                    bar4.SetActive(false);
                    bar3.SetActive(false);
                    bar2.SetActive(false);
                    break;

                case 0:
                    bar5.SetActive(false);
                    bar4.SetActive(false);
                    bar3.SetActive(false);
                    bar2.SetActive(false);
                    bar1.SetActive(false);
                    break;

                default:
                    break;
            }
        }

        lifes.text = player.Lifes.ToString();
    }

    
}
