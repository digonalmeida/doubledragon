﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsUI : MonoBehaviour {

    private Text _credits;

	private void Start ()
    {
        if (GameManager.Instance == null)
        {
            Destroy(this);
            return;
        }

        _credits = GetComponentInChildren<Text>();

        GameManager.Instance.OnCreditsChange = UpdateCredits;
        UpdateCredits(GameManager.Instance.credits);
    }

    private void UpdateCredits(int credits)
    {
        _credits.text = credits.ToString();
    }
}
