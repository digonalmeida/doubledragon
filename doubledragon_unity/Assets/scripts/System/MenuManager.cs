﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    public GameObject insertCoinLabel;
    public GameObject onePlayerLabel;
    public GameObject twoPlayersLabel;
    public GameObject pushStartLabel;
    public GameObject creditsLabel;
    public Graphic titleImage;

    public float counterTime = 1.0f;

    private float _counter;
    private bool _canChangeLayout = false;
    private bool _hasTitleAppeared = false;
    private bool _showingPushStart = false;

    void Start ()
    {
        insertCoinLabel.SetActive(false);
        onePlayerLabel.SetActive(false);
        twoPlayersLabel.SetActive(false);
        pushStartLabel.SetActive(false);
        creditsLabel.SetActive(false);

        _counter = counterTime;
        StartCoroutine(ShowTitle());

        GameManager.Instance.OnHasCredit = ChangeLayout;
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnHasCredit -= ChangeLayout;
    }

    void Update ()
    {
        if (!_hasTitleAppeared)
        {
            return;
        }

        if (GameManager.Instance.HasCredit)
        {
            UpdateLayout();
        }        
    }

    private void HasCredit()
    {
        _canChangeLayout = true;
    }
    
    private void ChangeLayout()
    {
        insertCoinLabel.SetActive(false);
        onePlayerLabel.SetActive(false);
        twoPlayersLabel.SetActive(false);
        pushStartLabel.SetActive(false);
        creditsLabel.SetActive(true);

        _canChangeLayout = false;
    }
    
    private void UpdateLayout()
    {
        if (_counter <= 0.0f)
        {
            if (_showingPushStart)
            {
                pushStartLabel.SetActive(true);

                onePlayerLabel.SetActive(false);
                twoPlayersLabel.SetActive(false);
            }
            else
            {
                pushStartLabel.SetActive(false);

                if (GameManager.Instance.credits > 1 && GameManager.Instance.hasCoop)
                {
                    onePlayerLabel.SetActive(false);
                    twoPlayersLabel.SetActive(true);
                }
                else
                {
                    onePlayerLabel.SetActive(true);
                    twoPlayersLabel.SetActive(false);
                }
            }

            _showingPushStart = !_showingPushStart;
            _counter = counterTime;
        }

        _counter -= Time.deltaTime;
    }

    private IEnumerator ShowTitle()
    {
        for (float f = 0.0f; f < 1f; f += 0.015f)
        {
            titleImage.color = new Color(titleImage.color.r, titleImage.color.r, titleImage.color.r, f);
            yield return null;
        }

        if (GameManager.Instance.credits == 0)
        {
            insertCoinLabel.SetActive(true);
        }

        _hasTitleAppeared = true;
    }
}
