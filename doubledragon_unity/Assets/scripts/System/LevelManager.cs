﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    private static LevelManager _Instance = null;
    public static LevelManager Instance
    {
        get
        {
            if (_Instance == null)
            {
                CreateInstance();
            }
            return _Instance;
        }
    }

    static void CreateInstance()
    {
        GameObject prefab = Resources.Load<GameObject>("LevelManager");
        if (prefab == null)
        {
            Debug.LogError("Couldn't find LevelManager prefab on resources");
        }

        GameObject instance = Instantiate(prefab);
        instance.name = "_LevelManager";
        instance.transform.SetAsFirstSibling();
        _Instance = instance.GetComponent<LevelManager>();
        if (_Instance == null)
        {
            Debug.LogError("LevelManager prefab has no LevelManager component!");
        }
    }


    public Character Player1;
    public Character Player2;

    public float gameoverSeconds = 5f;

    private bool _endingGame = false;
    public bool EndingGame 
    {
        get 
        {
            return _endingGame;

        }
    }

    void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Start ()
    {
        if (GameManager.Instance == null)
        {
            Destroy(this);
            return;
        }
    }
	
	void Update ()
    {
		if (IsDead(Player1) && IsDead(Player2) && !_endingGame)
        {
            StartCoroutine(EndGame());
        }
	}

    private bool IsDead(Character player)
    {
        return player == null || (player.Hp <= 0 && player.Lifes <= 0);
    }

    //private void EndGame()
    //{
    //    GameManager.Instance.GameOver();
    //}

    IEnumerator EndGame()
    {
        float timeLimit = Time.time + gameoverSeconds;
        bool itIsNotOverYet = false;

        _endingGame = true;

        while (timeLimit > Time.time)
        {
            if (!IsDead(Player1) || !IsDead(Player2))
            {
                itIsNotOverYet = true;
                break;
            }

            yield return null;
        }

        if (!itIsNotOverYet)
        {
            GameManager.Instance.GameOver();
        }

        _endingGame = false;
    }

}
