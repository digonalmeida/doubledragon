﻿
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private static GameManager _Instance = null;
    public static GameManager Instance
    {
        get
        {
            if(_Instance == null)
            {
                CreateInstance();
            }
            return _Instance;
        }
    }

    static void CreateInstance()
    {
        GameObject prefab = Resources.Load<GameObject>("GameManager");
        if(prefab == null)
        {
            Debug.LogError("Couldn't find GameManager prefab on resources");
        }

        GameObject instance = Instantiate(prefab);
        instance.name = "_GameManager";
        instance.transform.SetAsFirstSibling();
        _Instance = instance.GetComponent<GameManager>();
        if(_Instance == null)
        {
            Debug.LogError("GameManager prefab has no GameManager component!");
        }
    }
    
    public delegate void ActionPlusCredit(int credits);
    public delegate void ActionHasCredit();
    public delegate void ActionGamesStarted();
    public delegate void ActionGamesOver();
    public delegate void ActionKeyPressed();
    public ActionPlusCredit OnCreditsChange;
    public ActionHasCredit OnHasCredit;
    public ActionGamesStarted OnGameStarted;
    public ActionGamesStarted OnGameOver;
    public ActionKeyPressed OnStart1KeyPressed;
    public ActionKeyPressed OnStart2KeyPressed;
    public ActionKeyPressed OnCreditKeyPressed;

    public string menuScene;
    public string startScene;

    public bool hasCoop;
    public int credits = 0;

    public bool HasCredit 
    {
        get
        {
            return credits > 0; ;
        }
    }

    public bool isPlaying;

    public KeyCode start1Key;
    public KeyCode start2Key;
    public KeyCode creditKey;

    public bool start1KeyPressed;
    public bool start2KeyPressed;
    public bool creditKeyPressed;

    void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
        }
        else
        {
            Destroy(this);
        }

        DontDestroyOnLoad(gameObject);
    }
	
	void Update ()
    {
        ReadInput();
        UpdateGame();
    }

    private void ReadInput()
    {
        creditKeyPressed = Input.GetKeyDown(creditKey);
        start1KeyPressed = Input.GetKeyDown(start1Key);
        start2KeyPressed = Input.GetKeyDown(start2Key);
    }

    private void UpdateGame()
    {
        if (creditKeyPressed)
        {
            credits += 1;

            if (credits == 1)
            {
                if (OnHasCredit != null)
                {
                    OnHasCredit();
                }
            }

            if (OnCreditsChange != null)
            {
                OnCreditsChange(credits);
            }
        }

        if (isPlaying)
        {
            ReadInputPlaying();
        }
        else
        {
            ReadInputNotPlaying();
        }
    }

    private void ReadInputNotPlaying()
    {
        if (start1KeyPressed)
        {
            if (GameManager.Instance.credits != 0)
            {
                credits -= 1;
                StartGame();
            }

            if (OnStart1KeyPressed != null)
            {
                OnStart1KeyPressed();
            }
        }
        else if (start2KeyPressed)
        {
            if (GameManager.Instance.credits != 0)
            {
                if (hasCoop)
                {
                    credits -= 2;
                    StartGame();
                }
            }

            if (OnStart2KeyPressed != null)
            {
                OnStart2KeyPressed();
            }
        }
    }

    private void ReadInputPlaying()
    {
        
    }


    private void StartGame()
    {
        isPlaying = true;

        SceneManager.LoadScene(startScene);

        if (OnGameStarted != null)
        {
            OnGameStarted();
        }
    }

    public void GameOver()
    {
        isPlaying = false;

        SceneManager.LoadScene(menuScene);

        if (OnGameOver != null)
        {
            OnGameOver();
        }
    }
}
