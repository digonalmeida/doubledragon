﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public Character GuiCharacter;
    public Text hpText;
    
    void Update()
    {
        hpText.text = "Hp: " + GuiCharacter.Hp.ToString();
    }
}
